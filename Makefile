CC=gcc
CFLAGS=-Wall -g -lm

all: main

main: areas.c main.c 
	$(CC) $(CFLAGS) areas.c main.c -o main

clean:
	rm -f main
