int main() {
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void tri(double base, double altura){
    printf("%.2lf\n",(base * altura) / 2);
}

void retan(double base,double altura){
    printf("%.2lf\n",base * altura);
}

void quad(double lado){
    printf("%.2lf\n",lado * 4);
}

void circ(double raio){
    printf("%.2lf\n",M_PI*pow(raio,2.0));
}

return 0;
}
